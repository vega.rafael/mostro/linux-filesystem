#! /bin/sh

DATA_DEVICE=/dev/mmcblk0p3

# mount debug filesystem
mkdir -p /sys/kernel/debug
mount -t debugfs none /sys/kernel/debug

# mount user data partition 
mkdir -p /tmp/mnt/data
mount ${DATA_DEVICE} /tmp/mnt/data

# Load drivers
#modprobe uio_pruss
#modprobe snd-soc-tlv320aic3x
#modprobe snd-soc-davinci-mcasp
#modprobe snd_soc_simple_card
modprobe snd_seq_midi
alsactl restore

# Make sure presets file exists
if [ ! -e /tmp/mnt/data/presets.json ]; then
  sleep 1
  umount /tmp/mnt/data 
  mount ${DATA_DEVICE} /tmp/mnt/data
  cp /root/mostro/builtin-presets.json /tmp/mnt/data/presets.json
  sync
  umount /tmp/mnt/data 
  mount -o ro ${DATA_DEVICE} /tmp/mnt/data
fi 

/etc/priorities.sh

# Start the pd app
PD_COMMAND="pd -rt -nogui -alsamidi -midiindev 1 -midioutdev 1 -r 48000 -blocksize 128"
${PD_COMMAND} /root/mostro/mostro.beagle.pd &
sleep 1

# If Octave- and Octave+ were pressed during boot
BUTTONS=`cat /proc/cmdline`
if echo $BUTTONS | grep -q "buttons=yes"; then
   # Show "PC" in led display (i2c bus 1, address 20, registers 0, 1, 12 and 13)
   i2cset -y 1 0x20 0x00 0x00
   i2cset -y 1 0x20 0x01 0x00
   i2cset -y 1 0x20 0x12 0xCE
   i2cset -y 1 0x20 0x13 0x1D

   killall pd 

   # Load mass storage driver 
   umount /tmp/mnt/data 
   modprobe g_mass_storage file=${DATA_DEVICE} cdrom=0 stall=0 removable=1 nofua=1 iManufacturer=OuterSpaceSounds iProduct=MOSTRO
   exit
fi

# Modules needed for usb midi devices
modprobe snd_usb_audio
modprobe g_midi iProduct="MOSTRO" iManufacturer="OuterSpaceSounds"

# Start usb midi device monitor to make usb midi connections automatically.
nice -n 19 /usr/bin/midi-usb-monitor & 

# Start preset persistance script
nice -n 19 /root/mostro/save.presets.sh &

# If there is a "devel" directory
if [ -d "/tmp/mnt/data/devel" ]; then
  echo "Devel Mode" 
  killall midi-usb-monitor
  killall save.presets.sh
  killall pd 
  sleep 1

  # Start usb networking and ssh server (dropbear)
  rmmod g_midi
  modprobe g_ether # test!
  echo "nameserver 8.8.8.8" > /etc/resolv.conf
  ifdown usb0
  ifup usb0
  /etc/init.d/S50dropbear start
  umount /tmp/mnt/data 
  mount ${DATA_DEVICE} /tmp/mnt/data

  # Stop pd app and restart with patch from devel directory
  if [ -f "/tmp/mnt/data/devel/mostro.beagle.pd" ]; then
    ${PD_COMMAND} /tmp/mnt/data/devel/mostro.beagle.pd &
  else
    ${PD_COMMAND} /root/mostro/mostro.beagle.pd &
  fi
  nice -n 19 /usr/bin/midi-usb-monitor & 
  nice -n 19 /root/mostro/save.presets.sh &
fi

# # Are we running from a SD card? Flash the internal MMC.
# if [ -e /dev/mmcblk0 ]; then
#   # kills pd, creates partitions, copies files and powers off the machine
#   /etc/flash.mmc.sh
#   exit
# fi

# # Update firmware if a new image is available.
# if ls -A /tmp/mnt/data/MOSTRO_firmware_* 2> /dev/null; then
#   umount /tmp/mnt/data 
#   mount ${DATA_DEVICE} /tmp/mnt/data
#   # kills pd, extracts archive, checksums, overwrite files, reboots
#   /etc/firmware_updater.sh > /tmp/mnt/data/update_log.txt
#   exit
# fi


