#! /bin/sh

/etc/init.d/S50dropbear stop
rmmod g_ether
modprobe g_midi
killall pd
killall midi-usb-monitor
PD_COMMAND="pd -rt -nogui -noadc -alsamidi -midiindev 1 -midioutdev 1"
${PD_COMMAND} /root/mostro/mostro.beagle.pd &
midi-usb-monitor &
