#! /bin/sh

umount /tmp/mnt/data
mount /dev/mmcblk0p3 /tmp/mnt/data
killall pd
rmmod g_midi
modprobe g_ether
echo "nameserver 8.8.8.8" > /etc/resolv.conf
ifdown usb0
ifup usb0
/etc/init.d/S50dropbear start
