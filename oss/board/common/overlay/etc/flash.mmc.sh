#! /bin/sh

# Stop the app
killall pd
killall midi-usb-monitor
killall save.presets.sh

# turn on looper led.
echo 14 > /sys/class/gpio/export
echo out > /sys/class/gpio/gpio14/direction
echo 1 > /sys/class/gpio/gpio14/value

# unmount mmc partitions (if any)
umount /dev/mmcblk1p1
umount /dev/mmcblk1p2
umount /dev/mmcblk1p3

# partition and format internal mmc
# Using sed so we can document fdisk's one letter commands.
sed -e 's/\s*\([\+0-9a-zA-Z]*\).*/\1/' << EOF | fdisk /dev/mmcblk1
  o # clear the mmc partition table
  n # new partition
  p # primary partition
  1 # partition number 1
    # default - start at beginning of disk 
  +537M # 512MB boot parttion
  t # change partition type (chooses partition number 1 automatically)
  c # FAT32
  n # new partition
  p # primary partition
  2 # partion number 2
    # default, start immediately after preceding partition
  +537M # 512M root partition
  t # change partition type
  2 # partition number 2
  83 # Linux
  n # new partition
  p # primary partition
  3 # partion number 3
    # default, start immediately after preceding partition
    # default, rest of disk, that's 3GB data partition
  t # change partition type
  3 # partition number 3
  c # FAT32
  a # make a partition bootable
  1 # bootable partition is partition 1
  p # print the in-memory partition table
  w # write the partition table
  q # quit
EOF

# format the new partions as FAT32
mkfs.vfat -n BOOT /dev/mmcblk1p1
mkfs.vfat -n MOSTRO /dev/mmcblk1p3

# Copy files to boot partition 
mkdir -p /tmp/mnt/boot_sd
mount /dev/mmcblk0p1 /tmp/mnt/boot_sd
mkdir -p /tmp/mnt/boot_mmc
mount /dev/mmcblk1p1 /tmp/mnt/boot_mmc
cp /tmp/mnt/boot_sd/* /tmp/mnt/boot_mmc
sync
umount /tmp/mnt/boot_sd
umount /tmp/mnt/boot_mmc

# Copy files to root partition 
dd if=/dev/mmcblk0p2 of=/dev/mmcblk1p2

# done
poweroff

