#! /bin/sh -e

KERNEL_DIR=../../linux-kernel
#KERNEL_VERSION=4.19.73-ti-rt-r29
KERNEL_VERSION=4.19.82-bone-rt-r41

# Get bootloader from ../bootloader repo
cp -rf ../../bootloader/bin/* ${BINARIES_DIR}

# Get kernel that was built in the ../linux-kernel repo
cp -rf ${KERNEL_DIR}/deploy/${KERNEL_VERSION}.zImage ${BINARIES_DIR}/zImage

# Get additional kernel modules from ../linux-kernel. The modules we actually need are listed
# In modules-list.txt
rm -rf ${TARGET_DIR}/lib/modules
rm -rf ${TARGET_DIR}/lib/firmware
MODULES_SOURCE_DIR=${KERNEL_DIR}/deploy/${KERNEL_VERSION}.modules/lib/modules/${KERNEL_VERSION}
MODULES_TARGET_DIR=${TARGET_DIR}/lib/modules/${KERNEL_VERSION}
mkdir -p ${MODULES_TARGET_DIR}
#rsync --files-from ../oss/board/common/modules-list.txt ${MODULES_SOURCE_DIR} ${MODULES_TARGET_DIR}
cp -rf ${MODULES_SOURCE_DIR}/* ${MODULES_TARGET_DIR}/

# Get PRUIO library and PD externals from ../pru
# mkdir -p ${TARGET_DIR}/usr/include
mkdir -p ${TARGET_DIR}/usr/lib
mkdir -p ${TARGET_DIR}/usr/lib/pd/extra/beaglebone
cp -rf ../../pru/library/lib/* ${TARGET_DIR}/usr/lib
# cp ../../pru/library/include/* ${TARGET_DIR}/usr/include
cp -rf ../../pru/puredata-external/beaglebone.pd_linux ${TARGET_DIR}/usr/lib/pd/extra/beaglebone

# Get compiled device tree
cp -rf ../device-tree/am335x-bonegreen.dtb ${BINARIES_DIR}/

# Get alsamixer config
cp -rf ../../audio-drivers/asound.state ${TARGET_DIR}/var/lib/alsa

# Get mostro pd patch
mkdir -p ${TARGET_DIR}/root/mostro
cp -fr ../../pd/* ${TARGET_DIR}/root/mostro

# Get firmware updater script
cp -fr ../../firmware-updater/firmware_updater.sh ${TARGET_DIR}/etc
cp -fr ../../firmware-updater/leds_up.pd ${TARGET_DIR}/etc

# Get midi usb connection manager
cp -fr ../../midi-usb/midi-usb-monitor ${TARGET_DIR}/usr/bin

