PUREDATA_VERSION = 0.48-1
PUREDATA_SOURCE = 0.48-1.tar.gz
PUREDATA_SITE = https://github.com/pure-data/pure-data/archive
PUREDATA_CONF_OPTS = 
PUREDATA_DEPENDENCIES = alsa-lib
PUREDATA_INSTALL_STAGING = YES

# cd to puredata build directory and run ./autogen.sh before running ./configure
define PUREDATA_AUTOGEN
	(cd $(@D); $(TARGET_MAKE_ENV) ./autogen.sh)
endef
PUREDATA_PRE_CONFIGURE_HOOKS += PUREDATA_AUTOGEN

$(eval $(autotools-package))
