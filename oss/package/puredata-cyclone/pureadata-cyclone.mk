PUREDATA_CYCLONE_VERSION= 0.3beta2
PUREDATA_CYCLONE_SOURCE = cyclone0.3beta-2.tar.gz
PUREDATA_CYCLONE_SITE = https://github.com/porres/pd-cyclone/archive
PUREDATA_CYCLONE_DEPENDENCIES = puredata

define PUREDATA_CYCLONE_BUILD_CMDS
    $(MAKE) PDINCLUDEDIR=$(STAGING_DIR)/usr/include/pd $(TARGET_CONFIGURE_OPTS) -C $(@D) all
endef

define PUREDATA_CYCLONE_INSTALL_TARGET_CMDS
    $(MAKE) DESTDIR=$(TARGET_DIR) PDLIBDIR=/usr/lib/pd/extra $(TARGET_CONFIGURE_OPTS) -C $(@D) install
endef

$(eval $(generic-package))
