# Bootloader and Linux filesystem for MOSTRO.

This repository is a set of scripts and configuration files to set up the Linux root file system for the MOSTRO. This is equivalent to creating a tiny Linux distro for the Beaglebone from scratch. This is all based in [Buildroot](https://buildroot.org/) version 2019.02.6. Go read their docs. 

## Using the generated image. (This is probably what you're looking for)

The ouput of this whole repo is an image that you can burn into an SD card and boot the Beaglebone from it. After booting from the SD card, the whole thing (bootloader, kernel and filesystem) will be copied into the Beaglebone's internal MMC memory and you won't need the SD card anymore. Follow these steps:

1. Burn the image to a SD card by running the following commands, or grab the image from `buildroot/output/images/sdcard.img` and do whatever you like to burn it.
    
        cd /path/to/this/repo
        scripts/burn_to_sd.sh

2. Power off the MOSTRO, insert the SD card, press the button near the SD card, power the MOSTRO back on, release the button near the SD card, wait for the "Looper" LED to light up, wait for the MOSTRO to power itself off. 

3. Remove the SD card.

4. Power on the MOSTRO, it should boot from the internal MMC.

## Generating the image.

Rafa has a virtual machine set up to do all this. Use the virtual machine instead of spending time configuring your computer to do all this.

To build the whole thing (takes really long the first time) you need to set up the other repositories as described in the following sections and then run the following commands. This will download a cross-compiler, compile linux system libraries, etc. etc. and will create the bootable image described in the previous section. This image will contain Puredata, the MOSTRO patch, and everything else that is needed for the MOSTRO to function.

    cd buildroot
    make BR2_EXTERNAL=../oss mostro_defconfig
    make menuconfig     # optional
    make savedefconfig  # optional, writes to oss/configs/mostro_defconfig
    make

There is also an alternative version of the file system that is more suitable for development. File system is read-write, it does not flash to internal memory automatically, etc. to build that one instead:

    cd buildroot
    make BR2_EXTERNAL=../oss mostro_debug_defconfig
    make menuconfig     # optional
    make savedefconfig  # optional, writes to oss/configs/mostro_debug_defconfig
    make

### Linux Kernel

You need a compiled Linux kernel build tree at ../linux-kernel. Get it from https://gitlab.com/vega.rafael/mostro/linux-kernel. See oss/board/mostro/post-build.sh

    cd ../linux-kernel
    build_kernel.sh
    tools/rebuild_modules.sh
    tools/rebuild_dtbs.sh

### PRUIO Library

Similarly, you need the compiled pruio library at ../pru. https://gitlab.com/vega.rafael/mostro/pru.

    cd ../pru
    cd library
    make
    cd ../puredata-external
    make

### Bootloader

The bootloader is barebox with some custom configuration. See ../bootloader repo. Same as above.

### Audio driver

Same as above, the audio driver repo needs to be at ../audio-driver and compiled

### MOSTRO Synth Puredata Patch

Same as above: ../pd

### Device Tree.

Both the Audio driver and the PRUIO library add entries to the default beaglebone green device tree. The scripts in the device-tree directory in this repo grab different pieces from the audio driver and pru repositories, create the complete device tree, compile it and use it in the created images


## Serial terminal

A hardware serial terminal is available. Use it if you need to debug the boot sequence. Use a FTDI cable https://elinux.org/Beagleboard:BeagleBone_Black_Serial. login: root  password: oss

## License

Copyright (C) 2015 Rafael Vega <contacto@rafaelvega.co>

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.  

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
