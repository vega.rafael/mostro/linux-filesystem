#! /bin/sh

if [ -z "$DEVICE" ]; then
  echo "You need to set the DEVICE envirnoment variable."
  echo "DEVICE=/dev/sde scripts/burn_to_sd.sh for example."
  echo "Be careful not to overwrite your hard drive!!!!"
  exit
fi

cd buildroot/output/images
sudo umount ${DEVICE}1
sudo umount ${DEVICE}2
sudo umount ${DEVICE}3
sudo dd if=sdcard.img of=${DEVICE} bs=1M
sync
sudo eject ${DEVICE}

